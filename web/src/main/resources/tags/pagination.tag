<%@ tag language="java" pageEncoding="UTF-8"  body-content="scriptless"%>
<%@ attribute name="pageResult" required="true" type="ldh.common.PageResult"%>
<%@ attribute name="pageView" required="true" type="java.lang.Integer"%>
<%@ attribute name="pageSize" required="false" type="java.lang.Integer"%>
<%@ attribute name="action" required="true"%>
<%@ attribute name="formId" required="true"%>
<%@ attribute name="method" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="length" value="${pageSize != null ? pageSize : pageResult.pageSize }"/>
<c:set var="left" value="${pageView/2}"/>
<c:set var="right" value="${pageView - left}"/>
<c:set var="leftTotal" value="${pageResult.pageNo - 1}"/>
<c:set var="rightTotal" value="${pageResult.pageTotal - pageResult.pageNo - 1 }"/>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<%
	long first = ldh.common.util.PaginationUtil.getFirstPage(pageResult, pageView);
	long end = ldh.common.util.PaginationUtil.getEndPage(pageResult, pageView);
	request.setAttribute("first", first);
	request.setAttribute("end", end);
%>

<div class="pagination-tool">
	<form name="${formId }" id="${formId }" action="${action }" method="${method != null ? method : 'GET' }">
		<input type="hidden" name="pageNo" id="${formId }_pageNo" value="${pageResult.pageNo }"/>
		<jsp:doBody/>
		<div class="message">共<i class="blue">${pageResult.total }</i>条记录，当前显示第&nbsp;<i class="blue">${pageResult.pageNo }&nbsp;</i>页</div>
		<nav aria-label="Page navigation">
			<ul class="pagination">
				<li class="page-item"><a href="javascript:$paginationFirst('${formId }')" class="page-link">首页</a></li>
				<c:forEach var="page" begin="${first}" end="${end }">
					<li class="page-item" <c:if test="${pageResult.pageNo == page }">class="active"</c:if>><a href="javascript:$pagination('${formId }', ${page })" class="page-link">${page }</a></li>
				</c:forEach>

				<li class="page-item"><a href="javascript:$pagination('${formId }', ${pageResult.pageTotal })" class="page-link">尾页</a></li>

				<li class="page-item"><span class="page-size">每页数量
				<select name="pageSize" id="${formId }_pageSize" onChange="$paginationSelect('${formId }')">
					<option value="${pageResult.pageSize }">请选择</option>
					<option value="5" <c:if test="${pageResult.pageSize == 5 }">selected</c:if>>5</option>
					<option value="10" <c:if test="${pageResult.pageSize == 10 }">selected</c:if>>10</option>
					<option value="20" <c:if test="${pageResult.pageSize == 20 }">selected</c:if>>20</option>
					<option value="30" <c:if test="${pageResult.pageSize == 30 }">selected</c:if>>30</option>
				</select></span>
				</li>
			</ul>
		</nav>
	</form>
</div>

