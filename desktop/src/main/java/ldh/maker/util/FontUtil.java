package ldh.maker.util;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.AffineTransform;

public class FontUtil {

    public static Double getStringWidth(String str) {
        Font font=new Font("宋体",Font.BOLD,22);
        FontRenderContext frc = new FontRenderContext(new AffineTransform(),true,true);
        Rectangle rec = font.getStringBounds(str, frc).getBounds();
        return rec.getWidth() + 30;
    }

    public static void main(String[] args) {
        String str = "创建时间";
        double width = getStringWidth(str);
        System.out.println("www:" + width);
    }
}
