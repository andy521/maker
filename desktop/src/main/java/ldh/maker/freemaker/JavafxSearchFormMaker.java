package ldh.maker.freemaker;

import ldh.database.Table;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxSearchFormMaker extends BeanMaker<JavafxSearchFormMaker> {

    protected Table table;
    protected String projectPackage;

    public JavafxSearchFormMaker table(Table table) {
        this.table = table;
        return this;
    }

    public JavafxSearchFormMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    @Override
    public JavafxSearchFormMaker make() {
        data();
        out(ftl, data);

        return this;
    }

    @Override
    public void data() {
        fileName = table.getJavaName() + "SearchForm.fxml";
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("controllerPackage", pack);
    }
}
