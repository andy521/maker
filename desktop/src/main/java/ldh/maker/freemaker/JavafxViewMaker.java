package ldh.maker.freemaker;

import ldh.maker.database.TableInfo;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxViewMaker extends BeanMaker<JavafxViewMaker> {

    protected TableInfo tableInfo;
    protected String fxml;

    public JavafxViewMaker tableInfo(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
        return this;
    }

    public JavafxViewMaker fxml(String fxml) {
        this.fxml = fxml;
        return this;
    }

    @Override
    public JavafxViewMaker make() {
        data();
        out("spring/View.ftl", data);

        return this;
    }

    @Override
    public void data() {
        data.put("tableInfo", tableInfo);
        data.put("table", table);
        data.put("package", pack);
        data.put("fxml", fxml);
        data.put("className", className);
    }
}
