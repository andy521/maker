spring:
  application:
    name: eureka-server
  security:
    basic:
      enabled: true
    user:
      name: test
      password: test

server:
  port: ${param.port?c}

eureka:
  instance:
    hostname: ${param.host}
  client:
    registerWithEureka: false
    fetchRegistry: false
    service-url:
       defaultZone: ${param.host}:${param.port?c}
