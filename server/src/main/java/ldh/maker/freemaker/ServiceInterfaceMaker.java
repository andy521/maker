package ldh.maker.freemaker;


import ldh.bean.util.BeanInfoUtil;
import ldh.common.PageResult;
import ldh.database.Column;
import ldh.database.UniqueIndex;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;


public class ServiceInterfaceMaker extends BeanMaker<ServiceInterfaceMaker> {
	
	private Class<?> bean;
	private BeanMaker<?> beanWhereMaker;
	
	public ServiceInterfaceMaker() {
		imports.add(PageResult.class.getName());
	}
	
	public ServiceInterfaceMaker bean(Class<?> bean) {
		this.bean = bean;
		this.imports.add(bean.getName());
		return this;
	}
	
	public ServiceInterfaceMaker beanWhereMaker(BeanMaker<?> beanWhereMaker) {
		this.beanWhereMaker = beanWhereMaker;
		this.imports.add(beanWhereMaker.getName());
		return this;
	}
	
	public void data() {
		check();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str=sdf.format(new Date());
		data.put("Author",author);
		data.put("DATA",str);
		data.put("Description", description);

		if (bean != null) {
			data.put("bean", bean.getSimpleName());
			data.put("beanWhere", bean.getSimpleName() + "Where");
			
		}
		Set<UniqueIndex> uis = this.table.getIndexies();
		if (uis != null) {
			for (UniqueIndex ui : uis) {
				for (Column c : ui.getColumns()) {
					if (!BeanInfoUtil.isBaseClass(c.getPropertyClass())) {
						this.imports(c.getPropertyClass());
					}
				}
			}
		}

		handleEnum();
		
		if (beanWhereMaker != null){
			data.put("bean", beanWhereMaker.getSimpleName());
			data.put("beanWhere", beanWhereMaker.getSimpleName() + "Where");
		}
		super.data();
	}
	
	public void check() {
		if (bean == null && beanWhereMaker == null) {
			throw new NullPointerException("bean or beanMaker is not null!!!");
		}
		super.check();
	}
	
	@Override
	public ServiceInterfaceMaker make() {
		data();
		out("service-interface.ftl", data);
		
		return this;
	}
	
	public static void main(String[] args) {
		String outPath = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\base\\ldh\\base\\make\\freemaker";
		
		new ServiceInterfaceMaker()
		 	.pack("ldh.base.make.freemaker")
		 	.outPath(outPath)
		 	.className("ScenicDao")
		 	.make();
		
	}

	
}
