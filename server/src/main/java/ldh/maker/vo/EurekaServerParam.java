package ldh.maker.vo;

import lombok.Data;

/**
 * Created by ldh on 2019/2/19.
 */
@Data
public class EurekaServerParam {

    private String projectPackage;
    private Integer port;
    private Integer num;
    private String host;

}
