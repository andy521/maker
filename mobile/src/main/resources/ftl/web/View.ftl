<template>
  <div class="table-responsive ">
    <h1>详情页</h1>
    <table class="table table-bordered">
      <thead>
        <th width="200">名称</th>
        <th>值</th>
      </thead>
      <tbody>
        <tr v-for="(value,key) in objectData">
          <td align="right">{{key}}</td>
          <td>{{value}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</template>

<script>
  export default {
    name: 'edit',
    data: function() {
      return {
        objectData: {}
      }
    },
    mounted: function() { //mounted
        this.initData();
    },
    methods: {
        initData:function() {
            var that=this;
            $.ajax({
                type:"get",
                dataType:"JSON",
                crossDomain: true,
                url:"http://localhost:8282/${util.firstLower(table.javaName)}/view/json/" + this.$route.query.id,
                success:function(res){
                    that.objectData=res.data;
                }
            })
        }
    }
  }
</script>
